package com.master.noteapp.viewmodels;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.master.noteapp.models.noteModel;
import com.master.noteapp.repositories.repositories;

import java.util.List;


public class viewmodels extends AndroidViewModel {


    private repositories todoRepository;
    private LiveData<List<noteModel>> todos;

    public viewmodels(@NonNull Application application) {
        super(application);
        todoRepository = new repositories(application);
        todos = todoRepository.getTodos();
    }

    public void addTodo(noteModel todo) {
        this.todoRepository.addTodo(todo);
    }

    public void updateTodo(noteModel todo) {
        this.todoRepository.updateTodo(todo);
    }

    public void deleteTodo(noteModel todo) {
        this.todoRepository.deleteTodo(todo);
    }

    public void setCompletedTodo(String id) {
        this.todoRepository.setCompleted(id);
    }

    public void setUncompletedTodo(String id) {
        this.todoRepository.setUncompleted(id);
    }

    public LiveData<List<noteModel>> getTodosByCategory(String category) {
        return this.todoRepository.getTodosByCategory(category);
    }
    public LiveData<List<noteModel>> getTodos() {
        return todos;
    }
}
