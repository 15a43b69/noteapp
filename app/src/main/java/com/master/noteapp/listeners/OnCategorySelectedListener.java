package com.master.noteapp.listeners;


import com.master.noteapp.models.noteCategories;

public interface OnCategorySelectedListener {

    void categorySelected(noteCategories category) throws Exception;

}
