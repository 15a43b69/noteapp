package com.master.noteapp.listeners;


import com.master.noteapp.models.noteModel;

public interface OnTodoCompletedListener {

    void todoCompleted(noteModel todo);

}
