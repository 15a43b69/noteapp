package com.master.noteapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.master.noteapp.R;
import com.master.noteapp.listeners.OnTodoCompletedListener;
import com.master.noteapp.models.noteModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class noteAdapter extends RecyclerView.Adapter<noteAdapter.TodoViewHolder> {

    private List<noteModel> todos;
    private OnTodoCompletedListener mOnTodoCompletedListener;

    public noteAdapter(OnTodoCompletedListener mOnTodoCompletedListener) {
        this.mOnTodoCompletedListener = mOnTodoCompletedListener;
    }

    @NonNull
    @Override
    public TodoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View todoView = LayoutInflater.from(parent.getContext()).inflate(R.layout.note_item, parent, false);
        return new TodoViewHolder(todoView);
    }

    @Override
    public void onBindViewHolder(@NonNull TodoViewHolder holder, int position) {
        noteModel todo = todos.get(position);
        holder.setData(todo);
    }

    @Override
    public int getItemCount() {
        return todos.size();
    }

    public void setTodos(List<noteModel> todos) {
        this.todos = todos;
        notifyDataSetChanged();
    }

    class TodoViewHolder extends RecyclerView.ViewHolder {

        TextView title, datetime;
        CheckBox completed;

        public TodoViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.todoTitle);
            datetime = itemView.findViewById(R.id.todoDateTime);
            completed = itemView.findViewById(R.id.todoCompleted);

        }

        public void setData(noteModel todo) {
            this.title.setText(todo.getTitle());

            completed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        noteModel completedTodo = todos.get(getAdapterPosition());
                        mOnTodoCompletedListener.todoCompleted(completedTodo);
                    }
                }
            });
        }
    }

}
