package com.master.noteapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.master.noteapp.R;
import com.master.noteapp.models.noteModel;

import java.util.UUID;

public class AddActivity extends AppCompatActivity {

    private EditText todoTitle;
    private TextView todoCategory;
    private Button createTodoButton;

    private long createdAt;

    private String category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        todoTitle = findViewById(R.id.addTodoTitle);
        todoCategory = findViewById(R.id.addTodoCategory);
        createTodoButton = findViewById(R.id.createTodoButton);

        Bundle extras = getIntent().getExtras();
        category = extras.getString("category");

        todoCategory.setText(category);
        createdAt = System.currentTimeMillis();

        createTodoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String title = todoTitle.getText().toString();

                noteModel addedTodo = new noteModel(UUID.randomUUID().toString(), title, 0, category,"");

                Intent intent = new Intent();
                intent.putExtra("addedTodo", addedTodo);

                setResult(RESULT_OK, intent);
                finish();

            }
        });

    }
}
