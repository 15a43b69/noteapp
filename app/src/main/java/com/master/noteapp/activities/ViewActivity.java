package com.master.noteapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.master.noteapp.R;
import com.master.noteapp.adapters.noteAdapter;
import com.master.noteapp.listeners.OnTodoCompletedListener;
import com.master.noteapp.models.noteModel;
import com.master.noteapp.viewmodels.viewmodels;

import java.util.List;

public class ViewActivity extends AppCompatActivity {
    public static final String TAG = "TODOS";
    public static final int REQUEST_CODE = 1;
    private TextView viewCategoryTitle, viewCategoryTasks, noTodosContent, noTodosTitle;
    private ImageView viewCategoryImage, backArrow;

    private viewmodels ViewModel;
    private RecyclerView todosRecyclerView;
    private noteAdapter todoAdapter;

    private OnTodoCompletedListener mOnTodoCompletedListener = new OnTodoCompletedListener() {
        @Override
        public void todoCompleted(noteModel todo) {
            setTodoCompleted(todo);
        }
    };

    public void setTodoCompleted(final noteModel todo) {
        //remove the todo from the view
        ViewModel.deleteTodo(todo);
        //add the snackbar and the undo button
        Snackbar.make(findViewById(R.id.viewTodosContainer), "Todo Completed.", Snackbar.LENGTH_LONG)
                .setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ViewModel.addTodo(todo);
                    }
                }).show();
    }

    private FloatingActionButton addTodoButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        ViewModel = ViewModelProviders.of(this).get(viewmodels.class);

        viewCategoryTitle = findViewById(R.id.viewCategoryTitle);
        viewCategoryTasks = findViewById(R.id.viewCategoryTasks);
        viewCategoryImage = findViewById(R.id.viewCategoryImage);
        backArrow = findViewById(R.id.backArrow);
        noTodosTitle = findViewById(R.id.noTodosTitle);
        noTodosContent = findViewById(R.id.noTodosContent);
        todosRecyclerView = findViewById(R.id.todosList);
        addTodoButton = findViewById(R.id.addTodoFab);

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        addTodoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addTodoIntent = new Intent(ViewActivity.this, AddActivity.class);
                addTodoIntent.putExtra("category", viewCategoryTitle.getText().toString().toLowerCase());

                startActivityForResult(addTodoIntent, REQUEST_CODE);
            }
        });

        Bundle extras = getIntent().getExtras();

        String title = extras.getString("title");
        int image = extras.getInt("image");

        if (title.toLowerCase().equals("all")) {
            ViewModel.getTodos().observe(this, new Observer<List<noteModel>>() {
                @Override
                public void onChanged(List<noteModel> todos) {
                    if (todos.isEmpty()) {
                        noTodosTitle.setVisibility(View.VISIBLE);
                        noTodosContent.setVisibility(View.VISIBLE);
                        todosRecyclerView.setVisibility(View.INVISIBLE);

                        viewCategoryTasks.setText("0 tasks");
                    } else {
                        noTodosTitle.setVisibility(View.INVISIBLE);
                        noTodosContent.setVisibility(View.INVISIBLE);
                        todosRecyclerView.setVisibility(View.VISIBLE);

                        todoAdapter = new noteAdapter(mOnTodoCompletedListener);
                        todosRecyclerView.setHasFixedSize(true);
                        todosRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
                        todosRecyclerView.setAdapter(todoAdapter);

                        todoAdapter.setTodos(todos);
                        viewCategoryTasks.setText(todos.size() + " Task" + (todos.size() == 1 ? "" : "s"));
                    }
                }
            });
        } else {
            ViewModel.getTodosByCategory(title.toLowerCase()).observe(this, new Observer<List<noteModel>>() {

                @Override
                public void onChanged(List<noteModel> todos) {
                    if (todos.isEmpty()) {
                        noTodosTitle.setVisibility(View.VISIBLE);
                        noTodosContent.setVisibility(View.VISIBLE);
                        todosRecyclerView.setVisibility(View.INVISIBLE);

                        viewCategoryTasks.setText("0 tasks");
                    } else {
                        noTodosTitle.setVisibility(View.INVISIBLE);
                        noTodosContent.setVisibility(View.INVISIBLE);
                        todosRecyclerView.setVisibility(View.VISIBLE);

                        todoAdapter = new noteAdapter(mOnTodoCompletedListener);
                        todosRecyclerView.setHasFixedSize(true);
                        todosRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
                        todosRecyclerView.setAdapter(todoAdapter);

                        todoAdapter.setTodos(todos);
                        viewCategoryTasks.setText(todos.size() + " Task" + (todos.size() == 1 ? "" : "s"));
                    }
                }
            });
        }

        viewCategoryTitle.setText(title);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
                noteModel addedTodo = (noteModel) data.getSerializableExtra("addedTodo");
                ViewModel.addTodo(addedTodo);
            }
        }
    }
}