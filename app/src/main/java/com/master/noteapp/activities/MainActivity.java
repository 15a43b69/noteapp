package com.master.noteapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.master.noteapp.R;
import com.master.noteapp.adapters.CategoryAdapter;
import com.master.noteapp.listeners.OnCategorySelectedListener;
import com.master.noteapp.models.noteCategories;
import com.master.noteapp.models.noteModel;
import com.master.noteapp.viewmodels.viewmodels;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements OnCategorySelectedListener {

    private viewmodels ViewModel;
    private List<noteCategories> categories;
    int category1 = 0, category2 = 0, category3 = 0, category4;
    private RecyclerView mainListRecyclerView;
    private CategoryAdapter categoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ViewModel = ViewModelProviders.of(this).get(viewmodels.class);
        createCategories();

    }

    private void createCategories() {
        categories = new ArrayList<>();
        category1 = 0; category2 = 0; category3 = 0; category4 = 0;

        ViewModel.getTodos().observe(this, new Observer<List<noteModel>>() {
            @Override
            public void onChanged(List<noteModel> noteModels) {

                for (noteModel todo: noteModels) {
                    switch (todo.getCategory().toLowerCase()) {
                        case "category1": ++category1; break;
                        case "category2": ++category2; break;
                        case "category3": ++category3; break;
                        case "category4": ++category4; break;
                    }
                }

                categories.add(new noteCategories(R.attr.checkedIcon, "category1", category1));
                categories.add(new noteCategories(R.attr.checkedIcon, "category2", category2));
                categories.add(new noteCategories(R.attr.checkedIcon, "category3", category3));
                categories.add(new noteCategories(R.attr.checkedIcon, "category4", category4));

                mainListRecyclerView = findViewById(R.id.mainListsRecyclerView);
                categoryAdapter = new CategoryAdapter(categories, MainActivity.this);

                mainListRecyclerView.setHasFixedSize(true);
                mainListRecyclerView.setLayoutManager(new GridLayoutManager(MainActivity.this, 2));
                mainListRecyclerView.setAdapter(categoryAdapter);

            }
        });
    }

    @Override
    public void categorySelected(final noteCategories category) throws Exception {
        Intent intent = new Intent(MainActivity.this, ViewActivity.class);
        intent.putExtra("title", category.getTitle());
        intent.putExtra("image", category.getImageName());

        startActivity(intent);
    }
}