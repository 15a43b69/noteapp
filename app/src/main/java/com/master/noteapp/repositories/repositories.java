package com.master.noteapp.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.master.noteapp.Task.Task;
import com.master.noteapp.dao.noteDao;
import com.master.noteapp.databases.databases;
import com.master.noteapp.models.noteModel;

import java.util.List;

public class repositories {
    private noteDao dao;
    private databases Database;
    private LiveData<List<noteModel>> todos;

    public repositories(Application application) {
        Database = databases.getInstance(application);
        dao = Database.getDao();
        todos = dao.getTodos();
    }

    public void addTodo(noteModel todo) {
        new Task.AddTodo(dao).execute(todo);
    }

    public void updateTodo(noteModel todo) {
        new Task.UpdateTodo(dao).execute(todo);
    }

    public void deleteTodo(noteModel todo) {
        new Task.DeleteTodo(dao).execute(todo);
    }

    public void setCompleted(String id) {
        new Task.SetTodoCompleted(dao).execute(id);
    }

    public void setUncompleted(String id) {
        new Task.SetTodoUncompleted(dao).execute(id);
    }

    public LiveData<List<noteModel>> getTodosByCategory(String category) {

        LiveData<List<noteModel>> todos = null;

        try {
            Task.GetTodosByCategory getTodosByCategory = new Task.GetTodosByCategory(dao);
            todos = getTodosByCategory.execute(category).get();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return todos;

    }

    public LiveData<List<noteModel>> getTodos() {
        return todos;
    }
}
