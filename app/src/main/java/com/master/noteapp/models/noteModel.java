package com.master.noteapp.models;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.List;

@Entity(tableName = "note_table")
public class noteModel implements Serializable {

    @NonNull
    @PrimaryKey
    private String Id;
    private String title;
    private int completed;
    private String category;
    private String description;

    public noteModel(){}
    public noteModel(@NonNull String id, String title, int completed, String category, String description) {
        Id = id;
        this.title = title;
        this.completed = completed;
        this.category = category;
        this.description = description;
    }

    @NonNull
    public String getId() {
        return Id;
    }

    public void setId(@NonNull String id) {
        Id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCompleted() {
        return completed;
    }

    public void setCompleted(int completed) {
        this.completed = completed;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
