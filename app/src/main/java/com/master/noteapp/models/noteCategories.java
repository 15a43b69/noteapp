package com.master.noteapp.models;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.List;

@Entity(tableName = "categories_table")
public class noteCategories {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    private int id;

    private int imageName;

    private String title;

    private int tasks;

    public noteCategories() {
    }

    public noteCategories(int imageName, String title, int tasks) {
        this.imageName = imageName;
        this.title = title;
        this.tasks = tasks;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getImageName() {
        return imageName;
    }

    public void setImageName(int imageName) {
        this.imageName = imageName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTasks() {
        return tasks;
    }

    public void setTasks(int tasks) {
        this.tasks = tasks;
    }

}
