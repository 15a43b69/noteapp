package com.master.noteapp.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.master.noteapp.models.noteModel;

import java.util.List;


@Dao
public interface noteDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addTodo(noteModel todo);

    @Query("SELECT * FROM note_table WHERE category = :category ORDER BY id")
    LiveData<List<noteModel>> getTodosByCategory(String category);

    @Query("SELECT * FROM note_table ORDER BY id")
    LiveData<List<noteModel>> getTodos();

    @Delete
    void deleteTodo(noteModel todo);

    @Update
    void updateTodo(noteModel todo);

    @Query("UPDATE note_table SET completed = 1 WHERE id = :id")
    void setCompleted(String id);

    @Query("UPDATE note_table SET completed = 0 WHERE id = :id")
    void setUncompleted(String id);


}
