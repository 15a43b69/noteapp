package com.master.noteapp.Task;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.master.noteapp.dao.noteDao;
import com.master.noteapp.databases.databases;
import com.master.noteapp.models.noteModel;

import java.util.List;
import java.util.UUID;

public class Task {
    public static class AddTodo extends AsyncTask<noteModel, Void, Void> {

        noteDao dao;

        public AddTodo(noteDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(noteModel... noteModels) {
            this.dao.addTodo(noteModels[0]);
            return null;
        }
    }

    public static class UpdateTodo extends AsyncTask<noteModel, Void, Void> {

        noteDao dao;

        public UpdateTodo(noteDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(noteModel... todos) {
            this.dao.updateTodo(todos[0]);
            return null;
        }
    }

    public static class DeleteTodo extends AsyncTask<noteModel, Void, Void> {

        noteDao dao;

        public DeleteTodo(noteDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(noteModel... todos) {
            this.dao.deleteTodo(todos[0]);
            return null;
        }
    }
    public static class SetTodoCompleted extends AsyncTask<String, Void, Void> {

        noteDao dao;

        public SetTodoCompleted(noteDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(String... strings) {
            this.dao.setCompleted(strings[0]);
            return null;
        }
    }

    public static class SetTodoUncompleted extends AsyncTask<String, Void, Void> {

        noteDao dao;

        public SetTodoUncompleted(noteDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(String... strings) {
            this.dao.setUncompleted(strings[0]);
            return null;
        }
    }
    public static class PopulateTodos extends AsyncTask<Void, Void, Void> {

        noteDao dao;

        public PopulateTodos(databases db) {
            this.dao = db.getDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            this.dao.addTodo(new noteModel(UUID.randomUUID().toString(), "exp1",0, "", "exp2"));
            this.dao.addTodo(new noteModel(UUID.randomUUID().toString(), "exp2 exp2",0,"", "exp3"));
            this.dao.addTodo(new noteModel(UUID.randomUUID().toString(), "exp3",0, "", "exp4"));
            this.dao.addTodo(new noteModel(UUID.randomUUID().toString(), "exp4", 0,"","exp5"));
            return null;
        }
    }

    public static class GetTodosByCategory extends AsyncTask<String, Void, LiveData<List<noteModel>>> {

        noteDao dao;

        public GetTodosByCategory(noteDao dao) {
            this.dao = dao;
        }

        @Override
        protected LiveData<List<noteModel>> doInBackground(String... strings) {
            return this.dao.getTodosByCategory(strings[0]);
        }
    }
}
